Source: singularity-container
Section: admin
Priority: optional
Standards-Version: 4.6.2
Maintainer: Debian HPC Team <debian-hpc@lists.debian.org>
Uploaders: Mehdi Dogguy <mehdi@debian.org>,
           Benda Xu <orv@debian.org>
Build-Depends: debhelper-compat (= 13) ,dh-golang
    ,cryptsetup-bin
    ,golang-any (>= 2:1.20~)
    ,golang-github-acarl005-stripansi-dev
    ,golang-github-apex-log-dev
    ,golang-github-appc-cni-dev (>= 1.0.1~)
    ,golang-github-azure-go-ansiterm-dev
    ,golang-github-cenkalti-backoff-dev
    ,golang-github-containernetworking-plugins-dev
    ,golang-github-containerd-console-dev
    ,golang-github-containerd-continuity-dev
    ,golang-github-containerd-fifo-dev
    ,golang-github-containerd-go-cni-dev
    ,golang-github-containerd-stargz-snapshotter-dev
    ,golang-github-containerd-go-runc-dev
    ,golang-github-coreos-bbolt-dev
    ,golang-github-blang-semver-dev (>= 4)
    ,golang-github-buger-goterm-dev
    ,golang-github-buger-jsonparser-dev
    ,golang-github-russross-blackfriday-v2-dev
    ,golang-github-cespare-xxhash-dev
    ,golang-github-cilium-ebpf-dev
    ,golang-github-cloudflare-circl-dev
#   ,golang-github-containerd-containerd-dev (>= 1.2.6~) :: (provided by docker)
    ,golang-github-containers-image-dev (>= 5.26.1~)
    ,golang-github-containers-common-dev
    ,golang-github-containers-storage-dev
    ,golang-github-docker-docker-credential-helpers-dev
    ,golang-github-docker-libtrust-dev
    ,golang-github-felixge-httpsnoop-dev
    ,golang-github-gorilla-websocket-dev
    ,golang-github-gofrs-flock-dev
    ,golang-github-grpc-ecosystem-go-grpc-middleware-dev
    ,golang-github-go-log-log-dev
    ,golang-github-go-logr-logr-dev
    ,golang-github-go-logr-stdr-dev
    ,golang-github-golang-groupcache-dev
    ,golang-github-gotestyourself-gotest.tools-dev
    ,golang-github-google-shlex-dev
    ,golang-github-google-uuid-dev
    ,golang-github-gogo-googleapis-dev
    ,golang-github-golang-groupcache-dev
    ,golang-github-hashicorp-go-retryablehttp-dev (>= 0.6.2~)
    ,golang-github-hashicorp-go-immutable-radix-dev
    ,golang-github-mitchellh-go-homedir-dev
    ,golang-github-moby-locker-dev
    ,golang-github-moby-patternmatcher-dev
    ,golang-github-moby-term-dev
    ,golang-github-morikuni-aec-dev
    ,golang-mvdan-sh-dev
    ,golang-github-netflix-go-expect-dev
    ,golang-github-tonistiigi-fsutil-dev (>= 0.0~git20230630.36ef4d8-1~)
    ,golang-github-tonistiigi-units-dev
    ,golang-github-opencontainers-image-spec-dev (>= 1.1.0~rc4~)
    ,golang-github-opencontainers-runtime-tools-dev
    ,golang-github-opencontainers-selinux-dev
    ,golang-github-opencontainers-specs-dev
    ,golang-github-opensuse-umoci-dev (>= 0.4.7+ds-4~)
    ,golang-github-pelletier-go-toml.v2-dev
    ,golang-github-pkg-errors-dev
    ,golang-github-pquerna-ffjson-dev
    ,golang-github-protonmail-go-crypto-dev
    ,golang-github-safchain-ethtool-dev
    ,golang-github-samber-lo-dev
    ,golang-github-satori-go.uuid-dev
    ,golang-github-spf13-cobra-dev
    ,golang-github-spf13-pflag-dev
    ,golang-gopkg-square-go-jose.v2-dev
    ,golang-gopkg-yaml.v3-dev
    ,golang-github-sylabs-json-resp-dev
    ,golang-github-vbatts-tar-split-dev
    ,golang-github-vbauerster-mpb-dev (>= 8.6.1~)
#   ,golang-golang-x-crypto-dev :: (patched by upstream)
    ,golang-golang-x-term-dev
    ,golang-golang-x-text-dev
    ,golang-golang-x-sys-dev
    ,golang-gopkg-cheggaaa-pb.v1-dev
    ,help2man
    ,libssl-dev
    ,stow
    ,uuid-dev
Homepage: https://www.sylabs.io
Rules-Requires-Root: no
Vcs-Git: https://salsa.debian.org/hpc-team/singularity-container.git
Vcs-Browser: https://salsa.debian.org/hpc-team/singularity-container
XS-Go-Import-Path: github.com/sylabs/singularity

Package: singularity-container
Architecture: any
Built-Using: ${misc:Built-Using}
Depends: ${misc:Depends} ,${shlibs:Depends}
    ,ca-certificates
    ,containernetworking-plugins
    ,squashfs-tools
Recommends: e2fsprogs
            ,conmon
            ,runc
            ,squashfuse
Description: container platform focused on supporting "Mobility of Compute"
 Mobility of Compute encapsulates the development to compute model
 where developers can work in an environment of their choosing and
 creation and when the developer needs additional compute resources,
 this environment can easily be copied and executed on other platforms.
 Additionally as the primary use case for Singularity is targeted
 towards computational portability, many of the barriers to entry of
 other container solutions do not apply to Singularity making it an
 ideal solution for users (both computational and non-computational)
 and HPC centers.

Package: golang-github-sylabs-singularity-dev
Section: golang
Architecture: all
Depends: ${misc:Depends}
    ,golang-github-appc-cni-dev (>= 0.7.1~)
    ,golang-github-blang-semver-dev (>= 4)
    ,golang-github-containernetworking-plugins-dev
    ,golang-github-containers-image-dev (>= 5.26.1~)
    ,golang-github-gorilla-websocket-dev
    ,golang-github-opencontainers-image-spec-dev (>= 1.1.0~rc4~)
    ,golang-github-opencontainers-selinux-dev
    ,golang-github-opencontainers-specs-dev
    ,golang-github-opensuse-umoci-dev (>= 0.4.7+ds-4~)
    ,golang-github-pelletier-go-toml.v2-dev
    ,golang-github-satori-go.uuid-dev
    ,golang-github-spf13-cobra-dev
    ,golang-github-spf13-pflag-dev
    ,golang-github-sylabs-json-resp-dev
    ,golang-golang-x-crypto-dev
    ,golang-gopkg-cheggaaa-pb.v1-dev
    ,golang-gopkg-yaml.v2-dev
Description: development files for Singularity application container engine
 Mobility of Compute encapsulates the development to compute model
 where developers can work in an environment of their choosing and
 creation and when the developer needs additional compute resources,
 this environment can easily be copied and executed on other platforms.
 Additionally as the primary use case for Singularity is targeted
 towards computational portability, many of the barriers to entry of
 other container solutions do not apply to Singularity making it an
 ideal solution for users (both computational and non-computational)
 and HPC centers.
 .
 This package provides Golang sources for the Singularity API.
